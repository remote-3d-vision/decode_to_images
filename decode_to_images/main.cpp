#include <stdio.h>

#define __STDC_CONSTANT_MACROS

extern "C" {
#include <libswscale/swscale.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/pixdesc.h> //av_get_pix_fmt_name
}

#include "image_buffer/image_buffer.h"

#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "swscale.lib")

#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480
#define IMAGE_PIX_FMT PIX_FMT_BGRA

typedef struct decode_ctx_s
{
    AVFormatContext *fmt_ctx;
    AVCodecContext *decoder_ctx_left;
    AVCodecContext *decoder_ctx_right;
    int video_stream_index_left;
    int video_stream_index_right;

    image_buffer_writer image_writer_left;
    image_buffer_writer image_writer_right;
    image_info_t image_info_left;
    image_info_t image_info_right;

    //image buffers will be mapped to these frames 
    //these frames will hold the decoded, converted frames in the desired IMAGE_PIX_FMT
	AVFrame *image_frame_left[BUFFER_CHAIN_LENGTH];
	AVFrame *image_frame_right[BUFFER_CHAIN_LENGTH];

    //will be setup to convert decoded frame to IMAGE_PIX_FMT
    struct SwsContext *convert_ctx_left;
    struct SwsContext *convert_ctx_right;
} decode_ctx_t;

int decode_init(decode_ctx_t *decode_ctx, int output_width, int output_height, int output_pix_fmt)
{
    decode_ctx->fmt_ctx = NULL;
    decode_ctx->decoder_ctx_left = NULL;
    decode_ctx->decoder_ctx_right = NULL;
    decode_ctx->video_stream_index_left = -1;
    decode_ctx->video_stream_index_right = -1;

    //register all the codecs
    av_register_all();
    //init network components since we're using UDP
    avformat_network_init();

    if (decode_ctx->image_writer_left.init_left() < 0)
	{
        printf("Failed to initialize left image buffer writer\n");
        return -1;
	}
    if (decode_ctx->image_writer_right.init_right() < 0)
	{
        printf("Failed to initialize right image buffer writer\n");
        return -1;
	}

    //image_buffer has no idea about libav's pix formats
    decode_ctx->image_info_left.format = output_pix_fmt;
    decode_ctx->image_info_left.width = output_width;
    decode_ctx->image_info_left.height = output_height;
    //XXX this "*4" should be based on the format
    decode_ctx->image_info_left.size = output_width*output_height*4;
    decode_ctx->image_info_left.buffer_index = 0;
    decode_ctx->image_info_right.format = output_pix_fmt;
    decode_ctx->image_info_right.width = output_width;
    decode_ctx->image_info_right.height = output_height;
    decode_ctx->image_info_right.size = output_width*output_height*4;
    decode_ctx->image_info_right.buffer_index = 0;

    //map the shared memory buffers to AVFrames
    int i;
    for (i = 0; i < BUFFER_CHAIN_LENGTH; i++)
	{
        decode_ctx->image_frame_left[i] = av_frame_alloc();
        decode_ctx->image_frame_right[i] = av_frame_alloc();
        if (!decode_ctx->image_frame_left[i] || !decode_ctx->image_frame_right[i]) 
        {
            fprintf(stderr, "Could not allocate frame\n");
            return -1;
        }

       avpicture_fill((AVPicture *)(decode_ctx->image_frame_left[i]), 
                     (uint8_t *)(decode_ctx->image_writer_left.get_buffer(i)), //shared memory buffer
                   (AVPixelFormat)(decode_ctx->image_info_left.format), 
                   decode_ctx->image_info_left.width, 
                   decode_ctx->image_info_left.height);
       avpicture_fill((AVPicture *)(decode_ctx->image_frame_right[i]), 
                     (uint8_t *)(decode_ctx->image_writer_right.get_buffer(i)), //shared memory buffer
                   (AVPixelFormat)(decode_ctx->image_info_right.format), 
                   decode_ctx->image_info_right.width, 
                   decode_ctx->image_info_right.height);
	}

    return 0;
}

void decode_cleanup(decode_ctx_t *decode_ctx)
{
    if (decode_ctx->decoder_ctx_left)  
        avcodec_close(decode_ctx->decoder_ctx_left);
    if (decode_ctx->decoder_ctx_right)
        avcodec_close(decode_ctx->decoder_ctx_right);
    avformat_close_input(&(decode_ctx->fmt_ctx));
    int i;
    for (i = 0; i < BUFFER_CHAIN_LENGTH; i++)
	{
        av_free(decode_ctx->image_frame_left[i]);
        av_free(decode_ctx->image_frame_right[i]);
	}
    if (decode_ctx->convert_ctx_left)
        sws_freeContext(decode_ctx->convert_ctx_left);
    if (decode_ctx->convert_ctx_right)
        sws_freeContext(decode_ctx->convert_ctx_right);
}

int decode_open(decode_ctx_t *decode_ctx, char *source)
{
    //open input file, and allocate format context
    if (avformat_open_input(&(decode_ctx->fmt_ctx), source, NULL, NULL) < 0) 
    {
        fprintf(stderr, "Could not open source file %s\n", source);
        return -1;
    }

    //retrieve stream information
    if (avformat_find_stream_info(decode_ctx->fmt_ctx, NULL) < 0) 
    {
        fprintf(stderr, "Could not find stream information\n");
        goto error;
    }

    int i;
    //find the video streams, assume first is left and second (if available) is right
    for (i = 0; i < decode_ctx->fmt_ctx->nb_streams; i++)
    {
        if (decode_ctx->fmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            if (decode_ctx->video_stream_index_left < 0)
            {
                decode_ctx->video_stream_index_left = i;
            }
            else
            {
                decode_ctx->video_stream_index_right = i;
                break;
            }
        }
    }

    fprintf(stdout, "Found %d video streams\n", decode_ctx->fmt_ctx->nb_streams);

    //setup of the left
    decode_ctx->decoder_ctx_left = decode_ctx->fmt_ctx->streams[decode_ctx->video_stream_index_left]->codec;

    decode_ctx->decoder_ctx_left->flags |= CODEC_FLAG_LOW_DELAY;

    AVCodec *decoder_left;
    //find the left decoder
    decoder_left = avcodec_find_decoder(decode_ctx->decoder_ctx_left->codec_id);
    if (!decoder_left) 
    {
        fprintf(stderr, "Failed to find left video codec\n");
        goto error;
    }

    //open the left codec
    if (avcodec_open2(decode_ctx->decoder_ctx_left, decoder_left, NULL) < 0) 
    {
        fprintf(stderr, "Failed to open left video codec\n");
        goto error;
    }

    decode_ctx->convert_ctx_left = sws_getContext(decode_ctx->decoder_ctx_left->width, decode_ctx->decoder_ctx_left->height, decode_ctx->decoder_ctx_left->pix_fmt, 
		                                        decode_ctx->image_info_left.width, decode_ctx->image_info_left.height, (AVPixelFormat)decode_ctx->image_info_left.format, 
												SWS_BICUBIC, NULL, NULL, NULL);

    //setup the right if available
    if (decode_ctx->video_stream_index_right >= 0)
	{
        AVCodec *decoder_right;
        decode_ctx->decoder_ctx_right = decode_ctx->fmt_ctx->streams[decode_ctx->video_stream_index_right]->codec;

        decode_ctx->decoder_ctx_right->flags |= CODEC_FLAG_LOW_DELAY;

        //find the right decoder
        decoder_right = avcodec_find_decoder(decode_ctx->decoder_ctx_right->codec_id);
        if (!decoder_right) 
        {
            fprintf(stderr, "Failed to find right video codec\n");
            exit(1);
        }

        //open the right codec
        if (avcodec_open2(decode_ctx->decoder_ctx_right, decoder_right, NULL) < 0) 
        {
            fprintf(stderr, "Failed to open right video codec\n");
            exit(1);
        }


        decode_ctx->convert_ctx_right = sws_getContext(decode_ctx->decoder_ctx_right->width, decode_ctx->decoder_ctx_right->height, decode_ctx->decoder_ctx_right->pix_fmt, 
                                                    decode_ctx->image_info_right.width, decode_ctx->image_info_right.height, (AVPixelFormat)decode_ctx->image_info_right.format, 
                                                    SWS_BICUBIC, NULL, NULL, NULL);
	}

    return 0;

error:
    decode_cleanup(decode_ctx);
    return -1;
}


int decode_loop(decode_ctx_t *decode_ctx)
{
    //decoded frame from stream in stream's pixel format
    AVFrame *decoded_frame;
    AVPacket packet;
    int got_frame;

    //allocate memory for the decoded frame
    decoded_frame = av_frame_alloc();
    if (!decoded_frame) 
    {
        fprintf(stderr, "Could not allocate frame\n");
        return -1;
    }
    
    av_init_packet(&packet);
    packet.data = NULL;
    packet.size = 0;

    int ret;
	char error_buffer[AV_ERROR_MAX_STRING_SIZE] = {0};
    //keep reading frames until end of file / transmission
    while (av_read_frame(decode_ctx->fmt_ctx, &packet) >= 0) 
	{
        AVPacket orig_packet = packet;
        do 
		{
            if (packet.stream_index == decode_ctx->video_stream_index_left)
			{
                if ((ret = avcodec_decode_video2(decode_ctx->decoder_ctx_left, decoded_frame, &got_frame, &packet)) < 0) {
                    fprintf(stderr, "Error decoding video frame (%s)\n", av_make_error_string(error_buffer, AV_ERROR_MAX_STRING_SIZE, ret));
                    continue;
                }

                if (got_frame)
                {
                    fprintf(stdout, "Got left frame\n"); 

                    //convert the image
                    sws_scale(decode_ctx->convert_ctx_left, decoded_frame->data, decoded_frame->linesize, 0, decoded_frame->height, 
					          decode_ctx->image_frame_left[(decode_ctx->image_info_left.buffer_index + 1) % BUFFER_CHAIN_LENGTH]->data, 
							  decode_ctx->image_frame_left[(decode_ctx->image_info_left.buffer_index + 1) % BUFFER_CHAIN_LENGTH]->linesize);

                    decode_ctx->image_writer_left.write_image(&(decode_ctx->image_info_left));
				}
                packet.size -= ret;
                packet.data += ret;
			}
			else if (packet.stream_index == decode_ctx->video_stream_index_right)
			{
                if ((ret = avcodec_decode_video2(decode_ctx->decoder_ctx_right, decoded_frame, &got_frame, &packet)) < 0) {
                    fprintf(stderr, "Error decoding video frame (%s)\n", av_make_error_string(error_buffer, AV_ERROR_MAX_STRING_SIZE, ret));
                    continue;
                }

                if (got_frame)
                {
                    fprintf(stdout, "Got right frame\n"); 

                    //convert the image
                    sws_scale(decode_ctx->convert_ctx_right, decoded_frame->data, decoded_frame->linesize, 0, decoded_frame->height, 
						      decode_ctx->image_frame_right[(decode_ctx->image_info_right.buffer_index + 1) % BUFFER_CHAIN_LENGTH]->data, 
							  decode_ctx->image_frame_right[(decode_ctx->image_info_right.buffer_index + 1) % BUFFER_CHAIN_LENGTH]->linesize);

                    decode_ctx->image_writer_right.write_image(&(decode_ctx->image_info_right));
				}
                packet.size -= ret;
                packet.data += ret;
			}
        } while (packet.size > 0);
        av_free_packet(&orig_packet);
    }

    av_free(decoded_frame);
}


int main(int argc, char *argv[])
{
    decode_ctx_t decode_ctx;

    if (argc < 2)
    {
        printf("usage: %s video_source", argv[0]);
        return -1;
    }

    decode_init(&decode_ctx, IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_PIX_FMT);

    if (decode_open(&decode_ctx, argv[1]) < 0)
	{
        fprintf(stderr, "Failed to open decode context\n");
        getc(stdin);
        return -1;
	}

    decode_loop(&decode_ctx);

    decode_cleanup(&decode_ctx);

    return 0;
}